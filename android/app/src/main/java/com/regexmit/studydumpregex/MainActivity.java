package com.regexmit.studydumpregex;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import io.flutter.app.FlutterActivity;
import io.flutter.plugin.common.MethodCall;
import io.flutter.plugin.common.MethodChannel;
import io.flutter.plugin.common.MethodChannel.MethodCallHandler;
import io.flutter.plugins.GeneratedPluginRegistrant;

public class MainActivity extends FlutterActivity {

    private String sharedFile;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        GeneratedPluginRegistrant.registerWith(this);
        Intent intent = getIntent();
        String action = intent.getAction();
        String type = intent.getType();

        if (Intent.ACTION_SEND.equals(action) && type != null) {
            if ("application/pdf".equals(type) || "image/jpg".equals(type) || "image/jpeg".equals(type) || "image/png".equals(type) || "text/plain".equals(type) || "application/vnd.ms-powerpoint".equals(type) || "application/vnd.openxmlformats-officedocument.presentationml.presentation".equals(type) || "application/msword".equals(type) || "application/vnd.openxmlformats-officedocument.wordprocessingml.document".equals(type)) {
                try {
                    handleSendFile(intent);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        new MethodChannel(getFlutterView(), "app.channel.shared.data").setMethodCallHandler(new MethodCallHandler() {
            @Override
            public void onMethodCall(MethodCall call, MethodChannel.Result result) {
                if (call.method.contentEquals("getSharedFile")) {
                    result.success(sharedFile);
                    sharedFile = null;
                }
            }
        });
    }

    void handleSendFile(Intent intent) throws IOException {
        Uri fileUri = (Uri) intent.getParcelableExtra(Intent.EXTRA_STREAM);
        if (fileUri != null) {
            InputStream inStream = getContentResolver().openInputStream(fileUri);

            String extension = intent.getType().split("/")[1];

            File file = new File(getCacheDir(), "temporaryFile." + extension);
            try {
                try (OutputStream output = new FileOutputStream(file)) {
                    byte[] buffer = new byte[4 * 1024];
                    int read;

                    while ((read = inStream.read(buffer)) != -1) {
                        output.write(buffer, 0, read);
                    }

                    output.flush();
                }
            } finally {
                inStream.close();
            }

            sharedFile = file.getAbsolutePath();
        }
    }
}
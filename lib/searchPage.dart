import 'dart:convert';
import 'dart:async';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';

import 'models/StudyMaterial.dart';
import 'components/smTile.dart';
import 'components/fullPageStatic.dart';
import 'utils/api/search.dart';

class SearchPage extends StatefulWidget {
  final Function callbackDownloadStateSet;

  SearchPage({@required this.callbackDownloadStateSet});

  @override
  _SearchPageState createState() =>
      _SearchPageState(callbackDownloadStateSet: callbackDownloadStateSet);
}

class _SearchPageState extends State<SearchPage> {
  final Function callbackDownloadStateSet;

  _SearchPageState({this.callbackDownloadStateSet});

  List<StudyMaterial> searchResults = [];
  TextEditingController _queryController = new TextEditingController();

  bool _isLoading = false;

  void closeAndSetFilter(List<String> filters) {
    Navigator.pop(context, filters);
  }

  bool isGlobal = false;
  void toggleGlobalSearch(bool isGlobal) async {
    this.isGlobal = isGlobal;

    List<StudyMaterial> studyMaterialList =
        constructStudyMaterialList(await searchQuery(_queryController.text));
    setState(() {
      searchResults = studyMaterialList;
    });
  }

  DateTime lastRequestDateTime;
  Future<String> searchQuery(String inputQuery) async {
    setState(() {
      _isLoading = true;
    });

    Map<String, String> params = {
      's': inputQuery,
    };

    SharedPreferences preferences = await SharedPreferences.getInstance();
    String path = preferences.getString('Path');
    List<String> filters = path.split('/');

    if (!isGlobal) {
      params['year'] = filters[0];
      params['branch'] = filters[1];
      params['subject'] = filters[2];
    }

    http.Response searchResponse = await search(inputQuery, params);
    if (searchResponse.statusCode != 200) {
      return '';
    }
    return searchResponse.body;
  }

  List<StudyMaterial> constructStudyMaterialList(String fetchedSearchResult) {
    setState(() {
      _isLoading = false;
    });

    if (fetchedSearchResult == 'No upload found') {
      return [];
    }

    List<StudyMaterial> studyMaterialList = [];

    var decodedSearchResponse;
    try {
      decodedSearchResponse = json.decode(fetchedSearchResult);
    } catch (e) {
      return null;
    }

    decodedSearchResponse.forEach((map) async {
      StudyMaterial tempMaterial = studyMaterialFromResponse(map);
      studyMaterialList.add(tempMaterial);
    });
    return studyMaterialList;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: IconButton(
          icon: Icon(Icons.arrow_back),
          onPressed: () {
            Navigator.pop(context, null);
          },
        ),
        title: TextField(
          controller: _queryController,
          autofocus: true,
          decoration: InputDecoration(
              hintText: 'Search Query',
              focusedBorder: UnderlineInputBorder(
                  borderSide: BorderSide(color: Colors.white)),
              enabledBorder: UnderlineInputBorder(
                  borderSide: BorderSide(color: Colors.white))),
          onChanged: (query) async {
            lastRequestDateTime = DateTime.now();
            await Future.delayed(Duration(milliseconds: 500), () async {
              if (DateTime.now()
                      .difference(lastRequestDateTime)
                      .compareTo(Duration(milliseconds: 500)) >
                  0) {
                debugPrint('Sending search request with query: ' + query);
                var searchResponse = await searchQuery(query);

                debugPrint(
                    'Search response returned! (\n' + searchResponse + '\n)');
                List<StudyMaterial> studyMaterialList =
                    constructStudyMaterialList(searchResponse);
                setState(() {
                  searchResults = studyMaterialList;
                });
              }
            });
          },
        ),
        actions: <Widget>[
          GlobalChip(
            toggleGlobalSearch: toggleGlobalSearch,
          ),
          IconButton(
            icon: Icon(Icons.clear),
            onPressed: () {
              setState(() {
                _queryController.text = '';
              });
            },
          ),
        ],
      ),
      body: _queryController.text.isEmpty
          ? EnterSearchQuery()
          : _isLoading
              ? Center(child: CircularProgressIndicator())
              : searchResults == null
                  ? RetryIndicator()
                  : searchResults.length == 0
                      ? NoDocuments()
                      : ListView(
                          children: searchResults.map<Widget>((studyMaterial) {
                            return singleTile(
                                SMTileMode.search, context, studyMaterial,
                                closeAndSetFilter: closeAndSetFilter,
                                callbackDownloadStateSet:
                                    callbackDownloadStateSet);
                          }).toList(),
                        ),
    );
  }
}

class GlobalChip extends StatefulWidget {
  final Function(bool) toggleGlobalSearch;

  GlobalChip({@required this.toggleGlobalSearch});

  @override
  _GlobalChipState createState() => _GlobalChipState(toggleGlobalSearch);
}

class _GlobalChipState extends State<GlobalChip> {
  final Function(bool) toggleGlobalSearch;

  _GlobalChipState(this.toggleGlobalSearch);

  bool isGlobal = false;

  @override
  Widget build(BuildContext context) {
    return InputChip(
      label: Text('Global'),
      selected: isGlobal,
      onSelected: (globalSelected) {
        setState(() {
          isGlobal = globalSelected ? true : false;
          toggleGlobalSearch(globalSelected ? true : false);
        });
      },
    );
  }
}

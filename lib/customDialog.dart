import 'package:flutter/material.dart';

void customListDialog(
    {@required BuildContext context,
    @required String title,
    @required List<dynamic> listOfThings,
    @required Future<Function> tileFunction(String s)}) async {
  showDialog(
    context: context,
    builder: (context) {
      return SimpleDialog(
        title: Text(title),
        children: getSubjects(listOfThings, tileFunction),
      );
    },
  );
}

List<Widget> getSubjects(List<dynamic> listOfThings, Future<Function> tileFunction(String s)) {
  List<Widget> tobeReturned = listOfThings.map((s) {
    return ListTile(
      title: Text(s.toString()),
      onTap: () {
        tileFunction(s);
      },
    );
  }).toList();
  //tobeReturned.insert(0, Divider());
  return tobeReturned;
}

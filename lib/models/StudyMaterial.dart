import 'dart:convert';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_cache_manager/flutter_cache_manager.dart';
import 'package:http/http.dart' as http;
import 'package:path_provider/path_provider.dart';
import 'package:pref_dessert/pref_dessert.dart';
import 'package:studyDump/authFunctions.dart';
import '../utils/api/apiCalls.dart'; // For fetching `getSite` variable

class StudyMaterial {
  File thumbnail;
  String title;
  String fileType;
  int uploadTime;
  int size;
  String path;
  String downloadURL;
  String thumbnailURL;
  String id;
  int likes;

  StudyMaterial(
      {@required this.title,
      @required this.fileType,
      @required this.uploadTime,
      @required this.size,
      @required this.path,
      @required this.downloadURL,
      @required this.thumbnailURL,
      @required this.id,
      @required this.likes,
      this.thumbnail});
}

StudyMaterial studyMaterialFromResponse(Map<String, dynamic> studyMaterialMap) {
  return new StudyMaterial(
      title: studyMaterialMap['FileName'],
      fileType: studyMaterialMap['FileType'].split('/').last,
      uploadTime: studyMaterialMap['uploadTime'],
      size: studyMaterialMap['Size'],
      path: studyMaterialMap['Filters']['Year'] +
          '/' +
          studyMaterialMap['Filters']['Branch'] +
          '/' +
          studyMaterialMap['Filters']['Subject'],
      downloadURL: getSite +
          'download?fURL=' +
          base64Encode(
              studyMaterialMap['DownloadURL'].toString().runes.toList()),
      thumbnailURL: getSite +
          'download?fURL=' +
          base64Encode(
              studyMaterialMap['ThumbnailURL'].toString().runes.toList()),
      id: studyMaterialMap['_id'],
      likes: studyMaterialMap['Counts']['LikeCount']);
}

class StudyMaterialDesSer extends DesSer<StudyMaterial> {
  @override
  StudyMaterial deserialize(String s) {
    List<String> t = s.split('|');
    return StudyMaterial(
        title: t[0],
        fileType: t[1],
        uploadTime: int.parse(t[2]),
        size: int.parse(t[3]),
        path: t[4],
        downloadURL: t[5],
        thumbnailURL: t[6],
        id: t[7],
        likes: int.parse(t[8]));
  }

  @override
  String serialize(StudyMaterial t) {
    return '${t.title}|${t.fileType}|${t.uploadTime.toString()}|${t.size.toString()}|${t.path}|${t.downloadURL}|${t.thumbnailURL}|${t.id}|${t.likes.toString()}';
  }

  @override
  String get key => 'StudyMaterial';
}

// Defining a custom cache manager for caching downloaded files
class StudyMaterialCacheManager extends BaseCacheManager {
  static const key = 'customCache';
  var p;

  StudyMaterialCacheManager._()
      : super(key,
            maxAgeCacheObject: Duration(days: 7),
            maxNrOfCacheObjects: 200,
            fileFetcher: _customHttpGetter);

  // Create new studymaterialcachemanager if not already exists
  static StudyMaterialCacheManager _instance;

  factory StudyMaterialCacheManager() {
    if (_instance == null) {
      _instance = new StudyMaterialCacheManager._();
    }
    return _instance;
  }

  @override
  Future<String> getFilePath() async {
    var directory = await getTemporaryDirectory();
    return directory.path;
  }

  static Future<FileFetcherResponse> _customHttpGetter(String url,
      {Map<String, String> headers}) async {
    return HttpFileFetcherResponse(await http.get(url, headers: {
      'Authorization': 'Bearer ' + await getToken(),
      'Content-Type': 'application/x-www-form-urlencoded',
    }));
  }
}

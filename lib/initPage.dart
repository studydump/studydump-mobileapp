import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:studyDump/helperFunctions.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';

import 'authFunctions.dart';
import 'chipsUI.dart';
import 'utils/fetchSubjects.dart';

class InitPage extends StatefulWidget {
  @override
  _InitPageState createState() => _InitPageState();
}

class _InitPageState extends State<InitPage> {
  bool showNextButton = false;
  bool showSubjectSelectionPage = false;
  bool isLoggingIn = false;
  GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey();
  PageController page = new PageController(initialPage: 0);
  TextEditingController nameController = new TextEditingController();
  TextEditingController emailController = new TextEditingController();
  GlobalKey<FormState> form = new GlobalKey();

  @override
  void initState() {
    super.initState();
    checkLogin();
  }

  void checkLogin() async {
    await Future.delayed(Duration(milliseconds: 100));
    String name = await FlutterSecureStorage().read(key: 'Name');
    debugPrint(name.toString());
    if (name != null) {
      setState(() {
        showSubjectSelectionPage = true;
      });
      debugPrint(name);
      page.animateToPage(2,
          duration: Duration(milliseconds: 300), curve: Curves.decelerate);
    }
  }

  List<Widget> pageChildren() {
    List<Widget> toReturn = [
      Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.all(32.0),
              child: Text('studyDump',
                  textScaleFactor: 3.0,
                  style: TextStyle(fontFamily: 'CutiveMono')),
            ),
            Padding(
              padding:
                  const EdgeInsets.only(top: 32.0, right: 32.0, left: 32.0),
              child: Text('Thanks for installing!',
                  textScaleFactor: 2.0,
                  style: TextStyle(fontFamily: 'CutiveMono')),
            ),
            Padding(
              padding: const EdgeInsets.only(left: 32.0, top: 8.0),
              child: FlatButton(
                  shape: BeveledRectangleBorder(
                      borderRadius: BorderRadius.circular(8.0)),
                  child: Text(
                    'Continue',
                    style: TextStyle(color: Colors.white),
                  ),
                  color: Colors.black,
                  onPressed: () {
                    page.nextPage(
                        duration: Duration(milliseconds: 300),
                        curve: Curves.decelerate);
                  }),
            ),
          ],
        ),
      ),
      Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.only(top: 32.0, right: 32.0, left: 32.0),
            child: Text('Please provide just a few details to prevent spam.',
                textScaleFactor: 2.0,
                style: TextStyle(fontFamily: 'CutiveMono')),
          ),
          Padding(
            padding: const EdgeInsets.all(32.0),
            child: Form(
                key: form,
                child: Column(
                  children: <Widget>[
                    TextFormField(
                      controller: nameController,
                      decoration: InputDecoration(hintText: 'Full Name'),
                      validator: (s) {
                        if (s.isEmpty) {
                          return 'Please enter a name';
                        }
                      },
                    ),
                    TextFormField(
                      controller: emailController,
                      inputFormatters: [EmailEditFormatter()],
                      decoration: InputDecoration(hintText: 'Email'),
                      validator: (s) {
                        String p =
                            r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
                        RegExp regex = new RegExp(p);
                        if (!regex.hasMatch(s.trim()))
                          return 'Please provide a valid email id';
                      },
                    )
                  ],
                ),
                autovalidate: false),
          ),
          Padding(
            padding: const EdgeInsets.only(left: 32.0, top: 8.0),
            child: FlatButton(
                shape: BeveledRectangleBorder(
                    borderRadius: BorderRadius.circular(8.0)),
                child: Text(
                  'Login',
                  style: TextStyle(color: Colors.white),
                ),
                color: Colors.black,
                disabledColor: Colors.black26,
                onPressed: isLoggingIn
                    ? null
                    : () async {
                        if (form.currentState.validate()) {
                          _scaffoldKey.currentState.showSnackBar(SnackBar(
                            content: Text('Logging in, please wait...'),
                          ));
                          setState(() {
                            isLoggingIn = true;
                          });
                          bool authComplete = await authWithToken(
                              name: nameController.text.trim(),
                              email: emailController.text.trim());
                          if (authComplete) {
                            print('Auth Is Complete');
                            await fetchSubjects().then((v) {
                              showSubjectSelectionPage = true;
                              setState(() {});
                              FocusScope.of(context)
                                  .requestFocus(new FocusNode());
                              page.nextPage(
                                  duration: Duration(milliseconds: 300),
                                  curve: Curves.decelerate);
                            });
                          } else
                            _scaffoldKey.currentState.showSnackBar(SnackBar(
                              content: Text(
                                  'Couldn\'t communicate with the servers. Please try again in some time and do let us know if this problem persists'),
                            ));
                          setState(() {
                            isLoggingIn = false;
                          });
                        }
                      }),
          ),
        ],
      ),
    ];
    if (showSubjectSelectionPage) {
      toReturn.add(ChipsUi(mode: MODE.init));
    }
    return toReturn;
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData(
        primaryColor: Colors.white,
        accentColor: Colors.black,
      ),
      home: Scaffold(
        key: _scaffoldKey,
        body: PageView(
          controller: page,
          children: pageChildren(),
          physics: NeverScrollableScrollPhysics(),
        ),
      ),
    );
  }
}

class EmailEditFormatter extends TextInputFormatter {
  @override
  TextEditingValue formatEditUpdate(
      TextEditingValue oldValue, TextEditingValue newValue) {
    if (newValue.text.endsWith('. '))
      return oldValue.copyWith(
        text: oldValue.text.toLowerCase() + '.',
        selection: TextSelection.fromPosition(
            TextPosition(offset: oldValue.text.length + 1)),
      );
    return newValue.copyWith(text: newValue.text.toLowerCase());
  }
}

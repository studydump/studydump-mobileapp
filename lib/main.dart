import 'dart:convert';
import 'dart:async';
import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/services.dart';
import 'package:flutter/scheduler.dart';
import 'package:http/http.dart' as http;
import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:marquee/marquee.dart';
import 'package:pref_dessert/pref_dessert.dart';
import 'package:flutter_pagewise/flutter_pagewise.dart';
import 'package:shimmer/shimmer.dart';
import 'package:studyDump/helperFunctions.dart';

import 'chipsUI.dart';
import 'initPage.dart';
import 'authFunctions.dart';
import 'searchPage.dart';
import 'demoPage.dart';
import 'models/StudyMaterial.dart';
import 'components/smTile.dart';
import 'components/fullPageStatic.dart';
import 'utils/api/lastModified.dart';
import 'utils/api/getVersion.dart';
import 'utils/api/listUploads.dart';
import 'utils/fetchSubjects.dart';
import 'utils/dialogs.dart';

void main() async {
  SharedPreferences preferences = await SharedPreferences.getInstance();
  bool _notFirstTime = preferences.getBool('NotFirstTime');
  if (_notFirstTime == null) {
    runApp(InitPage());
  } else {
    runApp(MyApp());
  }
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Study Dump',
      theme: ThemeData(
        primaryColor: Colors.white,
        accentColor: Colors.black,
      ),
      home: MyHomePage(title: 'Study Dump'),
    );
  }
}

bool filterExpanded = false;

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage>
    with SingleTickerProviderStateMixin {
  static const platform = const MethodChannel('app.channel.shared.data');

  @override
  void initState() {
    super.initState();

    SchedulerBinding.instance.addPostFrameCallback((_) {
      launchDemo();
      getSharedPdf();
    });
    checkVersionChange();

    downloadIndicatorAnimationController = AnimationController(
      duration: Duration(seconds: 2),
      lowerBound: 0.0,
      vsync: this,
    );
    downloadIndicatorAnimation = Tween(begin: 0.0, end: 1.0)
        .animate(downloadIndicatorAnimationController)
          ..addListener(() {
            setState(() {});
          });
  }

  launchDemo() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    bool demoShown = preferences.getBool('demoShown') ?? false;

    if (!demoShown) {
      Navigator.push(
        context,
        MaterialPageRoute(builder: (_) {
          return DemoPage();
        }),
      );
      preferences.setBool('demoShown', true);
    }
  }

  getSharedPdf() async {
    var sharedData = await platform.invokeMethod("getSharedFile");
    if (sharedData != null) {
      Navigator.push(context, MaterialPageRoute(builder: (_) {
        return ChipsUi(
          mode: MODE.share,
          scaffoldKey: scaffoldKey,
          pageController: _pageLoadController,
          refreshPathCallback: refreshFilterButton,
          sharedFilePath: sharedData,
          mainAreaScrollController: mainAreaScrollController,
          uploadInitCallback: uploadInitCallback,
        );
      }));
    }
  }

  checkVersionChange() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    String version = preferences.getString('version');

    http.Response versionResponse = await getVersion();
    if (versionResponse.statusCode != 200) return;

    String fetchedVersion = versionResponse.body;
    if (version == null) {
      await preferences.setString('version', fetchedVersion);
      return;
    }
    double storedVersion = double.tryParse(version.substring(0, 3));
    debugPrint(double.tryParse(fetchedVersion.substring(0, 3)).toString());
    if (double.tryParse(fetchedVersion.substring(0, 3)) > storedVersion) {
      showAlertDialog(context, DialogMode.yesNo,
          'An update has released! Please update to a newer version because things might break in this version. Do you want to be directed to the ${Platform.isIOS ? 'app store' : 'play store'}?',
          yesFunction: () {
        launch(
            'https://play.google.com/store/apps/details?id=com.mitregex.studydump');
        preferences.setString('version', fetchedVersion);
      });
    }
  }

  Future<bool> _onWillPop() async {
    if (mainAreaScrollController.offset == 0) {
      filterExpanded = !filterExpanded;
      await mainAreaScrollController.animateTo(500,
          duration: Duration(milliseconds: 500), curve: Curves.ease);
      return false;
    }
    return true;
  }

  Animation<double> downloadIndicatorAnimation;
  AnimationController downloadIndicatorAnimationController;

  bool _showIndicator = false;
  bool _isDownload = false;
  void uploadInitCallback(bool showIndicator, [double progress]) {
    if (progress != null && progress > 0.05) {
      setState(() {
        _isDownload = true;
      });
      downloadIndicatorAnimationController.animateTo(progress,
          curve: Curves.linearToEaseOut);
    }
    if (!showIndicator) {
      setState(() {
        _isDownload = false;
      });
      downloadIndicatorAnimationController.reset();
    }
    setState(() {
      this._showIndicator = showIndicator;
    });
  }

  String path;
  String displayedPath;
  SharedPreferences preferences;
  String token;
  String newToken;

  UniqueKey appBarKey = new UniqueKey();
  final GlobalKey<ScaffoldState> scaffoldKey = new GlobalKey<ScaffoldState>();
  ScrollController mainAreaScrollController =
      new ScrollController(keepScrollOffset: true, initialScrollOffset: 500);

  final _pageLoadController = new PagewiseLoadController(
    pageSize: 10,
    pageFuture: (index) async {
      return await getItems(index);
    },
  );

  Future<bool> checkFirstTime() async {
    token = await getToken();
    return true;
  }

  Future<bool> getPath() async {
    preferences = await SharedPreferences.getInstance();
    displayedPath = preferences.getString('Path');
    return true;
  }

  void refreshFilterButton() {
    setState(() {
      path = preferences.getString('Path');
    });
  }

  static Future<List<StudyMaterial>> getItems(pageIndex) async {
    SharedPreferences preferences;

    // Update list of subjects
    preferences = await SharedPreferences.getInstance();
    String _checkSubject = preferences.getString('DateTime');

    // Checking if time
    if (_checkSubject != null) {
      debugPrint(
          'Stored DateTime: ' + DateTime.parse(_checkSubject).toString());
      debugPrint('Current DateTime: ' + DateTime.now().toString());
      debugPrint('Stored DateTime - Current DateTime: ' +
          (DateTime.parse(_checkSubject).day - DateTime.now().day).toString() +
          ' days');
      if (DateTime.now()
              .difference(DateTime.parse(_checkSubject))
              .compareTo(Duration(days: 1)) >
          0) {
        fetchSubjects();
      }
    } else {
      await fetchSubjects();
    }

    // INIT
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    String path = sharedPreferences.getString('Path');
    List<String> filters = path.split('/');
    debugPrint('Current Filters: ' + path);
    http.Response response;

    //  To check if anything was updated in recent history and download the most recent list if it is so
    response = await getLastModified(filters);
    debugPrint(
        'Page Last Loaded: ${sharedPreferences.getInt('lastPageLoaded').toString()}');
    debugPrint('Last Modified: ${int.tryParse(response.body).toString()}');

    if (sharedPreferences.getInt('lastModified$path') == null) {
      await sharedPreferences.setInt('lastModified$path', -1);
    }

    if (sharedPreferences.getInt('lastPageLoaded$path') == null) {
      await sharedPreferences.setInt('lastPageLoaded$path', -1);
    }

    if (response.body != 'Connection issue' &&
        (sharedPreferences.getInt('lastModified$path') <
                int.tryParse(response.body) ||
            sharedPreferences.getInt('lastPageLoaded$path') < pageIndex)) {
      // To set lastModified in sharedPreferences
      if (sharedPreferences.getInt('lastModified$path') <
          int.tryParse(response.body)) {
        await sharedPreferences.setInt(
            'lastModified$path', int.tryParse(response.body));
        await sharedPreferences.remove('lastPageLoaded$path');
      }

      // To get new items
      response = await getListUploads(filters, pageIndex + 1);

      // Returns empty item when listUploads shows no documents found
      if (response.body == 'No documents found') {
        return [];
      }

      List<dynamic> responseMap = json.decode(response.body);
      debugPrint('Got Response: ' + responseMap.toString());

      List<StudyMaterial> toBeReturned = [];
      FuturePreferencesRepository repo =
          new FuturePreferencesRepository<StudyMaterial>(
              new StudyMaterialDesSer());

      if (pageIndex == 0) {
        // Reset list specific to this path if page index == 0
        StudyMaterial forDelete = studyMaterialFromResponse(responseMap[0]);
        repo.removeWhere(
            (studyMaterial) => studyMaterial.path == forDelete.path);
      }

      responseMap.forEach((map) async {
        StudyMaterial tempMaterial = studyMaterialFromResponse(map);
        // Add to current displayed list
        toBeReturned.add(tempMaterial);
        // Add to cache
        repo.save(tempMaterial);
      });

      await sharedPreferences.setInt(
          'lastPageLoaded$path',
          sharedPreferences.getInt('lastPageLoaded$path') == null
              ? 0
              : sharedPreferences.getInt('lastPageLoaded$path') + 1);

      debugPrint('Show Following StudyMaterial: ' + toBeReturned.toString());
      return toBeReturned;
    } else {
      FuturePreferencesRepository repo =
          new FuturePreferencesRepository<StudyMaterial>(
              new StudyMaterialDesSer());
      List<StudyMaterial> toBeReturned = await repo.findAllWhere(
          (studyMaterial) => studyMaterial.path == path.toLowerCase());
      debugPrint('Loading from Cache: ' + toBeReturned.toString());

      if (toBeReturned.length == 0) {
        if (response.body == 'Connection issue') {
          throw Exception;
        }
      }

      try {
        if (toBeReturned.length < 10) {
          return toBeReturned;
        }
        try {
          return toBeReturned
              .getRange(pageIndex * 10, (pageIndex * 10) + 10)
              .toList();
        } catch (e) {
          return toBeReturned
              .getRange(pageIndex * 10, toBeReturned.length)
              .toList();
        }
      } catch (e) {
        return [];
      }
    }
  }

  Widget shimmerWidget() => Container(
      width: double.infinity,
      padding: const EdgeInsets.symmetric(horizontal: 16.0, vertical: 16.0),
      child: Shimmer.fromColors(
        baseColor: Colors.grey[300],
        highlightColor: Colors.grey[100],
        child: Column(
          children: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
              .map((_) => Padding(
                    padding: const EdgeInsets.only(bottom: 8.0),
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Container(
                          width: 48.0,
                          height: 48.0,
                          color: Colors.white,
                        ),
                        Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 8.0),
                        ),
                        Expanded(
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Container(
                                width: double.infinity,
                                height: 8.0,
                                color: Colors.white,
                              ),
                              Padding(
                                padding:
                                    const EdgeInsets.symmetric(vertical: 2.0),
                              ),
                              Container(
                                width: double.infinity,
                                height: 8.0,
                                color: Colors.white,
                              ),
                              Padding(
                                padding:
                                    const EdgeInsets.symmetric(vertical: 2.0),
                              ),
                              Container(
                                width: 40.0,
                                height: 8.0,
                                color: Colors.white,
                              ),
                            ],
                          ),
                        )
                      ],
                    ),
                  ))
              .toList(),
        ),
      ));

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: _onWillPop,
      child: FutureBuilder(
          future: checkFirstTime(),
          builder: (_, snap) {
            if (!snap.hasData) return Container();
            return Scaffold(
              key: scaffoldKey,
              appBar: AppBar(
                key: appBarKey,
                bottom: PreferredSize(
                  child: _showIndicator
                      ? LinearProgressIndicator(
                          backgroundColor: Colors.white,
                          value: _isDownload
                              ? downloadIndicatorAnimation.value
                              : null,
                        )
                      : Container(),
                  preferredSize: Size(0, 0),
                ),
                /* leading: IconButton(
                icon: Icon(Icons.announcement),
                onPressed: () {
                  showModalBottomSheet(
                      context: context,
                      builder: (builder) {
                        return Container(
                          height: 350.0,
                          decoration: BoxDecoration(
                              color: Colors.white,
                              borderRadius: new BorderRadius.only(
                                  topLeft: const Radius.circular(20.0),
                                  topRight: const Radius.circular(20.0))),
                          child: CustomScrollView(
                            slivers: <Widget>[
                              SliverAppBar(
                                title: Text('Announcements'),
                                actions: <Widget>[
                                  IconButton(
                                    onPressed: () async {
//                                        var path = await FlutterDocumentPicker.openDocument(
//                                            params: FlutterDocumentPickerParams(
//                                                allowedFileExtensions: ['pdf', 'txt', 'ppt', 'pptx', 'doc', 'docx']));
//                                        var fileName = path.split('/').last;
//                                        var fileToUpload = await File.fromUri(Uri.file(path)).create();
                                    },
                                    icon: Icon(Icons.add),
                                  )
                                ],
                                automaticallyImplyLeading: false,
                                centerTitle: true,
                              ),
                            ],
                          ),
                        );
                      });
                },
              ),
               */
                title: Text(
                  'studyDump',
                  style: TextStyle(
                      fontFamily: 'CutiveMono',
                      fontWeight: FontWeight.bold,
                      fontSize: 22.0),
                ),
                centerTitle: false,
                actions: <Widget>[
                  Container(
                    width: MediaQuery.of(context).size.width / 3,
                    child: FutureBuilder<bool>(
                      future: getPath(),
                      builder: (context, snapshot) {
                        if (!snapshot.hasData) return Container();
                        return Container(
                          margin: EdgeInsets.only(right: 8.0),
                          child: MaterialButton(
                            child: Marquee(
                              text: displayedPath.split('/').join('>'),
                              style: TextStyle(fontFamily: 'CutiveMono'),
                              blankSpace: 30.0,
                              velocity: 24.0,
                            ),
                            onPressed: () async {
                              filterExpanded = !filterExpanded;
                              if (filterExpanded) {
                                mainAreaScrollController.animateTo(0,
                                    duration: Duration(milliseconds: 500),
                                    curve: Curves.ease);
                              } else {
                                mainAreaScrollController.animateTo(500,
                                    duration: Duration(milliseconds: 500),
                                    curve: Curves.ease);
                              }
                            },
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(8.0),
                                side: BorderSide(color: Colors.black45)),
                          ),
                        );
                      },
                    ),
                    margin: EdgeInsets.all(8.0),
                  )
                ],
              ),
              floatingActionButtonLocation:
                  FloatingActionButtonLocation.centerDocked,
              bottomNavigationBar: BottomAppBar(
                elevation: 16.0,
                child: ListTile(
                  title: Row(
                    children: <Widget>[
                      Hero(
                        child: Material(
                          shape: CircleBorder(),
                          color: Theme.of(context).primaryColor,
                          child: IconButton(
                              icon: Icon(
                                Icons.search,
                                color: Theme.of(context).accentColor,
                              ),
                              onPressed: () async {
                                List<String> filters = await Navigator.push(
                                    context,
                                    new MaterialPageRoute(builder: (context) {
                                  return SearchPage(
                                    callbackDownloadStateSet:
                                        uploadInitCallback,
                                  );
                                }));
                                if (filters != null) {
                                  refreshList(
                                      filters: filters,
                                      mainAreaScrollController:
                                          mainAreaScrollController,
                                      mode: MODE.upload,
                                      pageController: _pageLoadController,
                                      refreshPathCallback: refreshFilterButton);
                                }
                              }),
                        ),
                        tag: 'searchTag',
                      ),
                      Expanded(
                        child: Container(
                          height: 50.0,
                        ),
                      ),
                      IconButton(
                        icon: Icon(Icons.settings),
                        onPressed: () {
                          showDocsDialog(context: context);
                        },
                      )
                      /* IconButton(
                      icon: Icon(Platform.isAndroid
                          ? Icons.list
                          : CupertinoIcons.book),
                      onPressed: () async {
                        preferences = await SharedPreferences.getInstance();
                        String path = preferences.getString('Path');
                        List<String> pathList = path.split('/');
                        var subjectList =
                            json.decode(preferences.getString('choicesData'));
                        customListDialog(
                          context: context,
                          title: 'Choose subject:',
                          listOfThings: subjectList['Years'][pathList[0]]
                              ['Branches'][pathList[1]]['Subjects'],
                          tileFunction: (String subject) async {
                            SharedPreferences preferences =
                                await SharedPreferences.getInstance();
                            if (subject != null)
                              await preferences.setString(
                                  'Path',
                                  pathList[0] +
                                      '/' +
                                      pathList[1] +
                                      '/' +
                                      subject);
                            await preferences.remove('lastModified');
                            await preferences.remove('lastPageLoaded');
                            Navigator.pop(context);
                            _pageLoadController.reset();
                          },
                        );
                      },
                    ) */
                    ],
                  ),
                ),
              ),
              floatingActionButton: FloatingActionButton.extended(
                icon: Icon(Icons.add),
                onPressed: () async {
                  Navigator.push(context, MaterialPageRoute(builder: (_) {
                    return ChipsUi(
                      mode: MODE.upload,
                      scaffoldKey: scaffoldKey,
                      pageController: _pageLoadController,
                      refreshPathCallback: refreshFilterButton,
                      uploadInitCallback: uploadInitCallback,
                    );
                  }));
                  /* if (uploaded != null && uploaded) {
                  preferences = await SharedPreferences.getInstance();
                  await preferences.remove('lastModified');
                } */
                  // _pageLoadController.reset();
                },
                label: Text('Add Files'),
              ),
              body: CustomScrollView(
                controller: mainAreaScrollController,
                slivers: <Widget>[
                  SliverFixedExtentList(
                    delegate: SliverChildListDelegate.fixed([
                      Stack(
                        children: <Widget>[
                          Container(
                            height: 500,
                            child: ChipsUi(
                              mode: MODE.filter,
                              pageController: _pageLoadController,
                              mainAreaScrollController:
                                  mainAreaScrollController,
                              refreshPathCallback: refreshFilterButton,
                            ),
                          ),
                          IgnorePointer(
                            child: Container(
                              decoration: BoxDecoration(
                                gradient: LinearGradient(
                                  begin: FractionalOffset.topCenter,
                                  end: FractionalOffset.bottomCenter,
                                  colors: [
                                    Colors.grey.withOpacity(0.0),
                                    Colors.black26,
                                  ],
                                  stops: [0.98, 1.0],
                                ),
                              ),
                            ),
                          )
                        ],
                      )
                    ]),
                    itemExtent: 500,
                  ),
                  SliverFillRemaining(
                    child: RefreshIndicator(
                      child: PagewiseListView(
                        pageLoadController: _pageLoadController,
                        itemBuilder: (_, studyMaterial, index) {
                          return SMTile(
                            studyMaterial,
                            callbackDownloadStateSet: uploadInitCallback,
                          );
                        },
                        loadingBuilder: (_) {
                          return shimmerWidget();
                        },
                        noItemsFoundBuilder: (_) {
                          return Container(
                            padding: EdgeInsets.only(
                                top: MediaQuery.of(context).size.height / 3.5),
                            child: NoDocuments(),
                          );
                        },
                        retryBuilder: (_, callback) {
                          return Padding(
                            padding: EdgeInsets.only(
                                top: MediaQuery.of(context).size.height / 3.5),
                            child: RetryIndicator(),
                          );
                        },
                      ),
                      onRefresh: () async {
                        SharedPreferences preferences =
                            await SharedPreferences.getInstance();
                        await preferences
                            .remove('lastPageLoaded$displayedPath');
                        newToken = await getToken();
                        _pageLoadController.reset();
                        return;
                      },
                    ),
                  )
                ],
              ),
            );
          }),
    );
  }
}

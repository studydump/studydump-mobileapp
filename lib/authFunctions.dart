import 'dart:async';

import 'package:http/http.dart' as http;
import 'package:flutter_secure_storage/flutter_secure_storage.dart';

import 'utils/api/apiCalls.dart';

Future<http.Response> getAuthToken(String name, String email) async {
  //await Future.delayed(Duration(milliseconds: 100));
  //return http.Response('desc', 500);
  return http.post(
    getSite + 'getToken',
    headers: {'Content-Type': 'application/x-www-form-urlencoded'},
    body: {'name': name, 'email': email},
  );
}

Future<bool> authWithToken({String name, String email}) async {
  int retriesAllowed = 10;

  if (name == null) {
    name = await FlutterSecureStorage().read(key: 'Name');
    email = await FlutterSecureStorage().read(key: 'Email');
  }
  http.Response response = await getAuthToken(name, email);
  while (response.statusCode ~/ 100 != 2 && retriesAllowed-- != 0) {
    print(response.statusCode);
    response = await getAuthToken(name, email);
  }

  if (response.statusCode != 200) {
    return false;
  }

  await FlutterSecureStorage().write(key: 'AuthToken', value: response.body);
  await FlutterSecureStorage().write(key: 'Name', value: name);
  await FlutterSecureStorage().write(key: 'Email', value: email);
  //File authToken = new File((await getApplicationDocumentsDirectory()).path + '/authToken');
  //await authToken.writeAsString(response.body);
  print('::Stored JWT::');
  return true;
}

Future<String> getToken() async {
  //File authToken = new File((await getApplicationDocumentsDirectory()).path + '/authToken');
  return await FlutterSecureStorage()
      .read(key: 'AuthToken'); //authToken.readAsString();
}

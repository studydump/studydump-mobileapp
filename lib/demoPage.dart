import 'package:flutter/material.dart';

class DemoPage extends StatefulWidget {
  @override
  _DemoPageState createState() => _DemoPageState();
}

class _DemoPageState extends State<DemoPage> {
  PageController _pageController;

  @override
  void initState() {
    super.initState();

    _pageController = new PageController();
  }

  void goNextPage() {
    _pageController.nextPage(
        duration: Duration(milliseconds: 500), curve: Curves.ease);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: PageView(
        controller: _pageController,
        children: [
          DemoSingleton(
            'Tutorial',
            'Browse through our features!',
            'assets/images/studydump-demo-1.png',
            false,
            goNextPage,
          ),
          DemoSingleton(
            'Filter',
            'A filter represents your subject. The UI allows for quick-switching between filters!',
            'assets/images/studydump-demo-2.png',
            false,
            goNextPage,
          ),
          DemoSingleton(
            'Upload files',
            'Just select the filters & the file, name it such that it\'s easily identifiable, and upload!',
            'assets/images/studydump-demo-3.png',
            false,
            goNextPage,
          ),
          DemoSingleton(
            'Search',
            'Searches within current filter by default, and a global search option is provided. Search also acts like Regex patterns, go wild!',
            'assets/images/studydump-demo-4.png',
            false,
            goNextPage,
          ),
          DemoSingleton(
            'Share!',
            'You can share files from other apps directly to one of studyDump\'s filters',
            'assets/images/studydump-demo-5.png',
            true,
          ),
        ],
      ),
    );
  }
}

class DemoSingleton extends StatelessWidget {
  final String title;
  final String subtitle;
  final String imagePath;
  final Function onNext;
  final bool isLastPage;

  DemoSingleton(this.title, this.subtitle, this.imagePath,
      [this.isLastPage, this.onNext]);

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: <Widget>[
        Image.asset(
          imagePath,
          height: MediaQuery.of(context).size.height,
          fit: BoxFit.cover,
        ),
        Container(
          decoration: BoxDecoration(
              gradient: LinearGradient(
                  begin: FractionalOffset.topCenter,
                  end: FractionalOffset.bottomCenter,
                  colors: [
                Colors.transparent,
                Colors.white60,
                Colors.white,
                Colors.white
              ],
                  stops: [
                0.0,
                0.6,
                0.75,
                1.0
              ])),
        ),
        Container(
          margin: EdgeInsets.all(16.0),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.end,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Text(
                title,
                textScaleFactor: 3.0,
                style: TextStyle(fontFamily: 'CutiveMono'),
                textAlign: TextAlign.center,
              ),
              Text(
                subtitle,
                textScaleFactor: 1.4,
                style: TextStyle(fontFamily: 'CutiveMono'),
                textAlign: TextAlign.center,
              ),
              Container(
                height: 100.0,
              ),
            ],
          ),
        ),
        Column(
          mainAxisAlignment: MainAxisAlignment.end,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Container(
              height: 70.0,
              child: Row(
                children: <Widget>[
                  DemoButton(
                    'Exit Tutorial',
                    () {
                      Navigator.of(context).pop();
                    },
                    isLastPage ? false : true,
                  ),
                  isLastPage
                      ? Container()
                      : DemoButton('Next Page', onNext, true),
                ],
              ),
            ),
            Container(
              height: 30,
            ),
          ],
        ),
      ],
    );
  }
}

class DemoButton extends StatelessWidget {
  final String buttonText;
  final Function buttonFunction;
  final bool coverHalf;

  DemoButton(this.buttonText, this.buttonFunction, this.coverHalf);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.all(8.0),
      height: 70.0,
      width: coverHalf
          ? MediaQuery.of(context).size.width / 2 - 16.0
          : MediaQuery.of(context).size.width - 16.0,
      child: FlatButton(
        shape: BeveledRectangleBorder(borderRadius: BorderRadius.circular(8.0)),
        child: Text(buttonText, style: TextStyle(color: Colors.white)),
        color: Colors.black,
        onPressed: buttonFunction,
      ),
    );
  }
}

import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:flutter_downloader/flutter_downloader.dart';
import 'package:open_file/open_file.dart';
import 'package:path_provider/path_provider.dart';
import 'package:filesize/filesize.dart';
import 'package:studyDump/utils/dialogs.dart';

import '../helperFunctions.dart';
import '../authFunctions.dart';
import '../models/StudyMaterial.dart';
import '../components/smTile-like-button.dart';
import '../utils/api/report.dart';

enum SMTileMode { normal, search }
enum DropdownAction { info, report }

class SMTile extends StatefulWidget {
  final StudyMaterial studyMaterial;
  final Function callbackDownloadStateSet;

  SMTile(this.studyMaterial, {@required this.callbackDownloadStateSet});

  _SMTileState createState() => _SMTileState(studyMaterial,
      callbackDownloadStateSet: callbackDownloadStateSet);
}

Future<List<String>> findFilterFromPath(StudyMaterial studyMaterial) async {
  List<String> filters = studyMaterial.path.split('/');
  SharedPreferences pref = await SharedPreferences.getInstance();
  Map js = json.decode(pref.getString('choicesData'));
  Map temporaryMap = js;
  List temporaryList;
  for (int i = 0; i < 3; i++) {
    switch (i) {
      case 0:
        temporaryMap = temporaryMap['Years'];
        break;
      case 1:
        temporaryMap = temporaryMap[filters[0]]['Branches'];
        break;
      case 2:
        temporaryList = temporaryMap[filters[1]]['Subjects'];
        break;
    }

    switch (i) {
      case 0:
      case 1:
        temporaryMap.forEach((k, v) {
          if (k.toString().toLowerCase() == filters[i]) {
            filters[i] = k;
          }
        });
        break;
      case 2:
        temporaryList.forEach((v) {
          if (v[0].toString().toLowerCase() == filters[i]) {
            filters[i] = v[0];
          }
        });
        break;
    }
  }
  return filters;
}

Widget singleTile(
    SMTileMode mode, BuildContext context, StudyMaterial studyMaterial,
    {Function setLikesCallback,
    Function closeAndSetFilter,
    Function callbackDownloadStateSet}) {
  return ListTile(
    leading: FutureBuilder<File>(future: Future<File>(() async {
      return (await StudyMaterialCacheManager()
              .downloadFile(studyMaterial.thumbnailURL))
          .file;
    }), builder: (_, snapshot) {
      if (!snapshot.hasData)
        return Container(height: 60, width: 60);
      else
        return Image.file(
          snapshot.data,
          height: 60,
          width: 60,
        );
    }),
    title: Text(
      studyMaterial.title,
      style: TextStyle(color: Colors.black),
    ),
    subtitle: Text(
        '${filesize(studyMaterial.size)} | Likes - ${studyMaterial.likes.toString()}${mode == SMTileMode.search ? '\n' : ''}${mode == SMTileMode.search ? studyMaterial.path : ''}',
        style: TextStyle(color: Colors.black45)),
    //enabled: closeAndSetFilter == null,
    onTap: () async {
      if (mode == SMTileMode.search) {
        List<String> filters = await findFilterFromPath(studyMaterial);
        closeAndSetFilter(filters);
      }
      callbackDownloadStateSet(true);
      String saveToDirectory;
      if (Platform.isIOS) {
        saveToDirectory =
            (await getApplicationDocumentsDirectory()).path + '/StudyDump';
      } else {
        saveToDirectory =
            (await getExternalStorageDirectory()).path + '/StudyDump';
      }

      String savedFileName = studyMaterial.title
              .substring(0, studyMaterial.title.lastIndexOf('.')) +
          '-' +
          studyMaterial.id.substring(0, 6) +
          studyMaterial.title.substring(studyMaterial.title.lastIndexOf('.'));

      String filePath = saveToDirectory + '/' + savedFileName;

      // Check if file present in cache, otherwise download file
      try {
        File temp = File(filePath);
        if (temp.readAsBytesSync() == []) throw Exception;
        OpenFile.open(filePath);
        callbackDownloadStateSet(false);
      } catch (e) {
        if (!Directory(saveToDirectory).existsSync()) {
          Directory(saveToDirectory).createSync(recursive: true);
        }

        if (mode == SMTileMode.normal) {
          Scaffold.of(context).showSnackBar(SnackBar(
            content: Text(Platform.isIOS
                ? 'Downloading, your file will automatically open once downloaded'
                : 'Download started, please check notification for updates'),
          ));
        }

        String downloaderQueueId = await FlutterDownloader.enqueue(
            url: studyMaterial.downloadURL,
            savedDir: saveToDirectory,
            showNotification: true,
            openFileFromNotification: true,
            headers: {
              'Authorization': 'Bearer ' + await getToken(),
              'Content-Type': 'application/x-www-form-urlencoded',
            },
            fileName: savedFileName);
        debugPrint('Downloader Queue ID: ' + downloaderQueueId.toString());
        FlutterDownloader.registerCallback((id, status, progress) async {
          debugPrint(
              'Status of Download: ${status.value.toString()} | $progress');
          callbackDownloadStateSet(true, progress / 100);
          if (status.value == 3) {
            debugPrint('Downloaded! Path: ' + filePath);
            callbackDownloadStateSet(false);
            OpenFile.open(filePath);
          }
        });
        Future.delayed(Duration(minutes: 1))
            .then((_) => callbackDownloadStateSet(false));
      }
    },
    trailing: mode == SMTileMode.normal
        ? Row(
            mainAxisSize: MainAxisSize.min,
            children: [
              LikeButton(
                  studyMaterial: studyMaterial,
                  setLikesCallback: setLikesCallback),
              PopupMenuButton(
                icon: Icon(Icons.more_vert),
                onSelected: (value) async {
                  switch (value) {
                    case DropdownAction.info:
                      showFileInfoDialog(
                          context: context, studyMaterial: studyMaterial);
                      break;
                    case DropdownAction.report:
                      await showAlertDialog(context, DialogMode.yesNo,
                          'Do you want to report this file for misuse of service?',
                          yesFunction: () async =>
                              await report(studyMaterial.id));
                      break;
                  }
                },
                itemBuilder: (context) => <PopupMenuEntry<DropdownAction>>[
                  const PopupMenuItem<DropdownAction>(
                    value: DropdownAction.info,
                    child: Text('Info'),
                  ),
                  const PopupMenuItem<DropdownAction>(
                    value: DropdownAction.report,
                    child: Text('Report'),
                  ),
                ],
              )
            ],
          )
        : Container(
            width: 0,
            height: 0,
          ),
  );
}

class _SMTileState extends State<SMTile> {
  StudyMaterial studyMaterial;
  Function callbackDownloadStateSet;

  _SMTileState(this.studyMaterial, {@required this.callbackDownloadStateSet});

  void setLikesCallback(int likes) {
    setState(() {
      studyMaterial.likes = likes;
    });
  }

  @override
  Widget build(BuildContext context) {
    return singleTile(SMTileMode.normal, context, studyMaterial,
        setLikesCallback: setLikesCallback,
        callbackDownloadStateSet: callbackDownloadStateSet);
  }
}

import 'dart:async';

import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../models/StudyMaterial.dart';
import '../utils/api/updateLike.dart';

class LikeButton extends StatefulWidget {
  final StudyMaterial studyMaterial;
  final Function setLikesCallback;

  LikeButton({@required this.studyMaterial, @required this.setLikesCallback});

  @override
  _LikeButtonState createState() => _LikeButtonState(
      studyMaterial: studyMaterial, setLikesCallback: setLikesCallback);
}

class _LikeButtonState extends State<LikeButton> {
  StudyMaterial studyMaterial;
  Function setLikesCallback;

  _LikeButtonState(
      {@required this.studyMaterial, @required this.setLikesCallback});

  Future<bool> _getIfLiked() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    List<String> liked = preferences.getStringList('liked');
    if (liked == null) await preferences.setStringList('liked', []);
    if (liked.contains(studyMaterial.id)) return true;
    return false;
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder<bool>(
        future: _getIfLiked(),
        builder: (_, snapshot) {
          if (!snapshot.hasData)
            return IconButton(
              tooltip: '0',
              icon: Icon(
                Icons.favorite_border,
                color: Colors.grey[700],
              ),
              onPressed: () {},
            );
          return IconButton(
              tooltip: studyMaterial.likes.toString(),
              icon: Icon(
                snapshot.data ? Icons.favorite : Icons.favorite_border,
                color: Colors.grey[700],
              ),
              onPressed: () async {
                SharedPreferences preferences =
                    await SharedPreferences.getInstance();
                // If liked, unlike
                if (snapshot.data) {
                  List<String> liked = preferences.getStringList('liked');
                  liked.remove(studyMaterial.id);
                  await preferences.setStringList('liked', liked);
                  setLikesCallback(studyMaterial.likes - 1);
                  updateLike(studyMaterial.id, true).then((response) async {
                    if (response.statusCode == 200) {
                      return;
                    }
                    List<String> liked = preferences.getStringList('liked');
                    liked.add(studyMaterial.id);
                    await preferences.setStringList('liked', liked);
                    setLikesCallback(studyMaterial.likes + 1);
                  });
                } else {
                  List<String> liked = preferences.getStringList('liked');
                  liked.add(studyMaterial.id);
                  await preferences.setStringList('liked', liked);
                  setLikesCallback(studyMaterial.likes + 1);
                  updateLike(studyMaterial.id, false).then((response) async {
                    if (response.statusCode == 200) {
                      return;
                    }
                    List<String> liked = preferences.getStringList('liked');
                    liked.remove(studyMaterial.id);
                    await preferences.setStringList('liked', liked);
                    setLikesCallback(studyMaterial.likes - 1);
                  });
                }
              });
        });
  }
}

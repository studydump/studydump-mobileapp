import 'package:flutter/material.dart';

Widget centerIconWithString(
    {@required IconData icon, @required String string}) {
  return Center(
      child: Column(
    mainAxisAlignment: MainAxisAlignment.center,
    children: <Widget>[
      Icon(
        icon,
        size: 100.0,
      ),
      Padding(
        padding: const EdgeInsets.all(40.0),
        child: Text(
          string,
          style:
              TextStyle(fontFamily: 'CutiveMono', fontWeight: FontWeight.bold),
          textAlign: TextAlign.center,
        ),
      ),
    ],
  ));
}

class EnterSearchQuery extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return centerIconWithString(
        icon: Icons.search, string: 'Please enter a search query.');
  }
}

class NoDocuments extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return centerIconWithString(
        icon: Icons.delete_sweep,
        string:
            'No Documents were found with current Filters. Please check again later.');
  }
}

class RetryIndicator extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return centerIconWithString(
        icon: Icons.cloud_off,
        string:
            'There appears to be a problem communicating with the servers, please refresh');
  }
}

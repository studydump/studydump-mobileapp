import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:http_parser/http_parser.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:studyDump/authFunctions.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:image_picker/image_picker.dart';
import 'package:http/http.dart' as http;
import 'package:file_picker/file_picker.dart';
import 'package:flutter_pagewise/flutter_pagewise.dart';
import 'package:studyDump/initPage.dart';
import 'package:studyDump/utils/dialogs.dart';
import 'package:studyDump/utils/fetchSubjects.dart';

import 'utils/api/apiCalls.dart';
import 'main.dart';

enum MODE { upload, share, filter, init, announcement }

void refreshList(
    {@required List<String> filters,
    @required MODE mode,
    @required PagewiseLoadController pageController,
    @required ScrollController mainAreaScrollController,
    @required Function refreshPathCallback}) async {
  SharedPreferences preferences = await SharedPreferences.getInstance();
  String path = filters[0] + '/' + filters[1] + '/' + filters[2];
  if (path != null &&
      (mode == MODE.filter || mode == MODE.upload || mode == MODE.share)) {
    debugPrint('\n Setting new filter! \n\n');
    await preferences.setString('Path', path);
    await preferences.remove('lastPageLoaded$path');
    pageController.reset();

    if (mode == MODE.filter) {
      mainAreaScrollController.animateTo(500,
          duration: Duration(milliseconds: 500), curve: Curves.ease);
    }

    refreshPathCallback();
    filterExpanded = false;
  }
}

class ChipsUi extends StatefulWidget {
  final MODE mode;
  final PagewiseLoadController pageController;
  final ScrollController mainAreaScrollController;
  final Function refreshPathCallback;
  final GlobalKey<ScaffoldState> scaffoldKey;
  final Function uploadInitCallback;
  final String sharedFilePath;

  ChipsUi(
      {@required this.mode,
      this.pageController,
      this.mainAreaScrollController,
      this.refreshPathCallback,
      this.scaffoldKey,
      this.uploadInitCallback,
      this.sharedFilePath});

  @override
  _ChipsUiState createState() => _ChipsUiState(
      mode: mode,
      pageController: pageController,
      mainAreaScrollController: mainAreaScrollController,
      refreshPathCallback: refreshPathCallback,
      scaffoldKey: scaffoldKey,
      uploadInitCallback: uploadInitCallback,
      sharedFilePath: sharedFilePath);
}

class _ChipsUiState extends State<ChipsUi> {
  MODE mode;
  final PagewiseLoadController pageController;
  final ScrollController mainAreaScrollController;
  final Function refreshPathCallback;
  final GlobalKey<ScaffoldState> scaffoldKey;
  final String sharedFilePath;

  _ChipsUiState(
      {this.mode,
      this.pageController,
      this.mainAreaScrollController,
      this.refreshPathCallback,
      this.scaffoldKey,
      this.uploadInitCallback,
      this.sharedFilePath});

  GlobalKey<ScaffoldState> _chipsUIScaffoldKey;
  ScrollController _chipsUIScrollController;
  bool firstLoad = true;
  TextEditingController fileNameController =
      new TextEditingController(text: '');
  FocusNode fileNameFocusNode = FocusNode(canRequestFocus: true);
  String buttonText;
  String selectedFilePath;
  bool _validName = true;

  Widget fileNameField(TextEditingController nameController) {
    return TextField(
      focusNode: fileNameFocusNode,
      controller: nameController,
      textAlign: TextAlign.center,
      decoration: InputDecoration(
          border: OutlineInputBorder(
              borderSide: BorderSide(width: 2.0),
              borderRadius: BorderRadius.circular(3.0)),
          contentPadding: EdgeInsets.all(20.0),
          focusedBorder: OutlineInputBorder(
              borderSide: BorderSide(width: 2.0),
              borderRadius: BorderRadius.circular(3.0)),
          helperText: 'Hint: \'topic-reason-source\'',
          hintText: 'Desired display file name',
          errorText: _validName ? null : 'Please enter a name'),
      onSubmitted: goAhead(),
    );
  }

  @override
  void initState() {
    super.initState();
    _chipsUIScaffoldKey = new GlobalKey();
    _chipsUIScrollController = new ScrollController();

    if (mode == MODE.filter) {
      assert(pageController != null);
      assert(mainAreaScrollController != null);
      assert(refreshPathCallback != null);
    }

    if (mode == MODE.upload) {
      assert(scaffoldKey != null);
      assert(pageController != null);
      assert(refreshPathCallback != null);
      assert(uploadInitCallback != null);
    }

    if (mode == MODE.share) {
      assert(sharedFilePath != null);
    }

    if (mode == MODE.init) {
      checkNoSubjectData();
    }
  }

  void checkNoSubjectData() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    var subjectData;
    try {
      subjectData = json.decode(preferences.getString('choicesData'));
    } catch (e) {
      await fetchSubjects();
      setState(() {});
      return;
    }
    if (!(subjectData is Map<String, dynamic>)) {
      await fetchSubjects();
      runApp(InitPage());
    }
  }

  Map<String, String> contentType = {
    'pdf': 'application/pdf',
    'txt': 'text/plain',
    'ppt': 'application/vnd.ms-powerpoint',
    'pptx':
        'application/vnd.openxmlformats-officedocument.presentationml.presentation',
    'doc': 'application/msword',
    'docx':
        'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
    'png': 'image/png',
    'jpeg': 'image/jpeg',
    'jpg': 'image/jpeg',
  };

  bool isSupportedExtension(String filePath) {
    bool isSupported = false;
    String pseudoExtension = filePath.split('.').last;

    contentType.forEach((k, v) {
      if (k == pseudoExtension || v.contains(pseudoExtension)) {
        isSupported = true;
      }
    });
    return isSupported;
  }

  // File picker for all file types - Image, pdf, txt, ...
  pickFile({@required FileType type}) async {
    if (!Platform.isIOS)
      await PermissionHandler().requestPermissions([PermissionGroup.storage]);
    File fileToUpload;
    try {
      Future<String> path = FilePicker.getFilePath(type: type);

      bool _stopUploading = false;
      showDriveUploadDialog(context, onPressed: () {
        _stopUploading = true;
        Navigator.pop(context);
        Navigator.pop(context);
      });

      String filePathFinal;
      await path.then((filePath) {
        if (Navigator.canPop(context)) Navigator.pop(context);

        if (filePath == null) {
          return;
        }

        if (!isSupportedExtension(filePath)) {
          throw Exception;
        } else {
          filePathFinal = filePath;
        }
      });

      if (filePathFinal == null) {
        return;
      }

      fileToUpload = await File.fromUri(Uri.file(filePathFinal)).create();
      if (!_stopUploading && fileToUpload.existsSync()) {
        setState(() {
          buttonText = filePathFinal.split('/').last;
          selectedFilePath = filePathFinal;
        });
        fileNameFocusNode.requestFocus();
      } else {
        _stopUploading = false;
      }
    } catch (e) {
      print(e.toString());
      _chipsUIScaffoldKey.currentState.showSnackBar(SnackBar(
        content: Text(
            'We only support pdf\'s, doc\'s, ppt\'s, txt\'s, and images at the moment'),
      ));
    }

    fileToUpload = null;
  }

  Function uploadInitCallback;
  uploadThis(String fileName, List<String> filters, {File fileToUpload}) async {
    bool continueUpload = await showUploadConfirmation(
        context: context,
        fileName: fileName,
        filters: filters,
        onCancel: () {
          Navigator.pop(context, false);
        },
        onConfirm: () {
          Navigator.pop(context, true);
        });

    if (!continueUpload) {
      return;
    }

    print('Uploading File: ' + fileName);
    var uri = Uri.parse(getSite + 'uploadFile');
    var request = new http.MultipartRequest('POST', uri);
    request.headers.addAll({
      'Authorization': 'Bearer ' + await getToken(),
      'Content-Type': 'application/x-www-form-urlencoded'
    });
    request.fields['year'] = filters[0];
    request.fields['branch'] = filters[1];
    request.fields['subject'] = filters[2];
    request.fields['notif'] = 'false';

    String fileExtension;
    String fileType;
    if (mode == MODE.share) {
      fileType = fileToUpload.path.split('temporaryFile.').last;
      contentType.forEach((k, v) {
        if (v.contains(fileType)) {
          fileType = v;
          fileExtension = k;
        }
      });
    } else {
      fileExtension = fileToUpload.path.split('.').last;
      fileType = contentType[fileExtension];
    }

    request.files.add(new http.MultipartFile.fromBytes(
      'uploadFile',
      await fileToUpload.readAsBytes(),
      filename: '$fileName.$fileExtension',
      contentType: new MediaType.parse(fileType),
    ));

    // Go to main page
    if (Navigator.canPop(context)) Navigator.pop(context);
    scaffoldKey.currentState
        .showSnackBar(SnackBar(content: Text('Uploading...')));

    try {
      uploadInitCallback(true);
      request.send().then((response) async {
        String responseBody = await response.stream.bytesToString();
        if (response.statusCode == 200) {
          print('Uploaded!');
          SharedPreferences preferences = await SharedPreferences.getInstance();
          await preferences.remove('lastPageLoaded${filters.join('/')}');
          scaffoldKey.currentState.showSnackBar(SnackBar(
            content: Text('Uploaded!'),
            action: SnackBarAction(
              label: 'Open Filter',
              textColor: Colors.blueAccent,
              onPressed: () async {
                refreshList(
                    filters: filters,
                    mainAreaScrollController: mainAreaScrollController,
                    mode: mode,
                    pageController: pageController,
                    refreshPathCallback: refreshPathCallback);
              },
            ),
          ));
        } else if (response.statusCode == 404)
          scaffoldKey.currentState.showSnackBar(SnackBar(
            content: Text(
                'Coudn\'it upload because server endpoint wasn\'t found, please let us know of this fault'),
          ));
        else if (response.statusCode == 400)
          scaffoldKey.currentState.showSnackBar(SnackBar(
            content: Text(
                'Duplicate document found, please like that one instead, thanks!'),
          ));
        else if (response.statusCode == 500) if (responseBody
            .contains('Duplicate'))
          scaffoldKey.currentState.showSnackBar(SnackBar(
            content: Text(
                'Duplicate document found, please like that one instead, thanks!'),
          ));
        else
          scaffoldKey.currentState.showSnackBar(SnackBar(
            content: Text(
                'Couldn\'t upload due to internal server error, please try again in some time'),
          ));
        uploadInitCallback(false);
      });
    } catch (e) {
      debugPrint(e.toString());
      uploadInitCallback(false);
    }
  }

  Map js;
  List<String> filters = new List<String>(4);

  Iterable<Widget> dataWidgets(
      Map<String, dynamic> labels, int filterKey) sync* {
    List labelsList = [];
    if (filterKey == 2)
      js['Years'][filters[0]]['Branches'][filters[1]]['Subjects']
          .forEach((v) => labelsList.add(v));
    else
      labels.forEach((k, v) => labelsList.add(k));
    for (var someLabel in labelsList) {
      var label;
      String tooltip;
      if (someLabel is List) {
        label = mode == MODE.init ? someLabel[1] : someLabel[0];
        tooltip = someLabel[1];
      } else {
        label = someLabel;
        tooltip = someLabel;
      }

      yield Padding(
        padding: const EdgeInsets.only(left: 4.0, right: 4.0),
        child: Tooltip(
          message: tooltip,
          child: FilterChip(
            label: Text(label),
            selected: filters[filterKey] != null
                ? filters[filterKey] == label
                : false,
            onSelected: (bool value) async {
              setState(() {
                if (value) {
                  filters[filterKey] = label;
                  for (int i = filterKey + 1; i < filters.length; i++) {
                    filters[i] = null;
                  }
                  print(filters);
                }
              });

              Future.delayed(Duration(milliseconds: 50), () {
                double _maxScroll =
                    _chipsUIScrollController.position.maxScrollExtent;
                _chipsUIScrollController.animateTo(_maxScroll ?? 0,
                    duration: Duration(milliseconds: 500), curve: Curves.ease);
              });

              if (filterKey == 2 && mode == MODE.filter) {
                refreshList(
                    filters: filters,
                    mainAreaScrollController: mainAreaScrollController,
                    mode: mode,
                    pageController: pageController,
                    refreshPathCallback: refreshPathCallback);
              }
            },
          ),
        ),
      );
    }
  }

  Function goAhead() {
    return ([String fileName]) async {
      if (fileNameController.text.trim().isEmpty &&
          (mode == MODE.upload || mode == MODE.share)) {
        setState(() {
          _validName = false;
        });
        return;
      }
      setState(() {
        _validName = true;
      });

      if (mode == MODE.init) {
        SharedPreferences preferences = await SharedPreferences.getInstance();

        Map js = json.decode(preferences.getString('choicesData'));
        List subjectList =
            js['Years'][filters[0]]['Branches'][filters[1]]['Subjects'];
        String subject;

        for (List subjectInfo in subjectList) {
          if (subjectInfo[1] == filters[2]) {
            subject = subjectInfo[0];
          }
        }

        await preferences.setString(
            'Path', filters[0] + '/' + filters[1] + '/' + subject);
        print(preferences.getString('Path'));
        await preferences.setBool('NotFirstTime', true);
        runApp(MyApp());
      } else if (mode == MODE.share) {
        uploadThis(fileNameController.text.trim(), filters,
            fileToUpload: File(sharedFilePath));
      } else if (mode == MODE.upload) {
        try {
          uploadThis(fileNameController.text.trim(), filters,
              fileToUpload: File(selectedFilePath));
        } catch (e) {
          debugPrint(e.toString());
          _chipsUIScaffoldKey.currentState.showSnackBar(SnackBar(
            content: Text('Please select an image or a file!'),
          ));
        }
      } else {
        refreshList(
            filters: filters,
            mainAreaScrollController: mainAreaScrollController,
            mode: mode,
            pageController: pageController,
            refreshPathCallback: refreshPathCallback);
      }
    };
  }

  Future<bool> loadChoicesData() async {
    SharedPreferences pref = await SharedPreferences.getInstance();
    js = json.decode(pref.getString('choicesData'));
    if (mode != MODE.init && firstLoad) {
      firstLoad = !firstLoad;
      List<String> initFilters = (pref.getString('Path')).split('/');
      filters[0] = initFilters[0];
      filters[1] = initFilters[1];
      filters[2] = initFilters[2];
    }

    return true;
  }

  bool toShowOrNotToShow() {
    if (mode == MODE.filter) {
      return false;
    }

    if (filters[2] != null && mode == MODE.init) {
      return true;
    }

    if (filters[2] != null &&
        mode == MODE.share &&
        fileNameController.text.isNotEmpty) {
      return true;
    }

    if (buttonText != 'Select File' &&
        mode == MODE.upload &&
        fileNameController.text.isNotEmpty) {
      return true;
    }

    return false;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _chipsUIScaffoldKey,
      floatingActionButton: toShowOrNotToShow()
          ? FloatingActionButton(
              onPressed: goAhead(),
              child: Icon(
                mode == MODE.upload
                    ? Icons.cloud_upload
                    : mode == MODE.init ? Icons.done : Icons.share,
              ))
          : Container(),
      appBar: mode == MODE.init
          ? AppBar(
              title: Text('Initial subject to view'),
              centerTitle: true,
              leading: Container(),
            )
          : AppBar(
              title: Text(mode == MODE.upload
                  ? 'Add more study material'
                  : mode == MODE.share
                      ? 'Share study material'
                      : 'Filter Subjects'),
              centerTitle: true,
            ),
      body: FutureBuilder<bool>(
        future: loadChoicesData(),
        builder: (_, snap) {
          if (!snap.hasData) return Center(child: CircularProgressIndicator());
          return ListView(
            controller: _chipsUIScrollController,
            children: <Widget>[
              Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  ListTile(
                    title: Center(child: Text('Year')),
                  ),
                  Wrap(
                    children: dataWidgets(js['Years'], 0).toList(),
                  ),
                  Divider(),
                  filters[0] != null
                      ? ListTile(
                          title: Center(child: Text('Branch')),
                        )
                      : Container(),
                  filters[0] != null
                      ? Wrap(
                          children: dataWidgets(
                                  js['Years'][filters[0]]['Branches'], 1)
                              .toList(),
                        )
                      : Container(),
                  filters[0] != null ? Divider() : Container(),
                  filters[1] != null
                      ? ListTile(
                          title: Center(child: Text('Subject')),
                        )
                      : Container(),
                  filters[1] != null
                      ? Wrap(
                          children: dataWidgets(
                                  js['Years'][filters[0]]['Branches'], 2)
                              .toList(),
                        )
                      : Container(),
                  filters[1] != null
                      ? mode == MODE.upload ? Divider() : Container()
                      : Container(),
                  filters[2] != null && mode == MODE.upload
                      ? Padding(
                          padding: EdgeInsets.symmetric(horizontal: 16.0),
                          child: buttonText == null
                              ? Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: <Widget>[
                                    RaisedButton(
                                      child: Text('Select Image'),
                                      onPressed: () {
                                        pickFile(type: FileType.IMAGE);
                                      },
                                    ),
                                    Container(width: 10.0),
                                    RaisedButton(
                                      child: Text('Select File'),
                                      onPressed: () {
                                        pickFile(type: FileType.ANY);
                                      },
                                    ),
                                  ],
                                )
                              : RaisedButton(
                                  padding: EdgeInsets.all(10.0),
                                  child: Text(buttonText),
                                  onPressed: () {
                                    setState(() {
                                      buttonText = null;
                                      selectedFilePath = null;
                                    });
                                  },
                                ),
                        )
                      : filters[2] != null && mode == MODE.share
                          ? Padding(
                              padding: EdgeInsets.all(16.0),
                              child: fileNameField(fileNameController),
                            )
                          : Container(),
                  Padding(
                    padding: const EdgeInsets.all(16.0),
                    child: filters[2] != null && mode == MODE.upload
                        ? fileNameField(fileNameController)
                        : Container(),
                  ),
                ],
              )
            ],
          );
        },
      ),
    );
  }
}

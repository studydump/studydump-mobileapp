import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart' show rootBundle;
import 'package:intl/intl.dart';
import 'package:share/share.dart';
import 'package:url_launcher/url_launcher.dart';

import '../demoPage.dart';
import '../models/StudyMaterial.dart';

class Consts {
  Consts._();

  static const double padding = 16.0;
  static const double avatarRadius = 55.0;
}

void showDemo(BuildContext context) {
  Navigator.pop(context);
  Navigator.push(context, MaterialPageRoute(builder: (_) {
    return DemoPage();
  }));
}

void showCredits(BuildContext context) {
  Navigator.pop(context);
  showDialog(
      context: context,
      builder: (context) {
        return Dialog(
          elevation: 0.0,
          backgroundColor: Colors.transparent,
          child: Stack(
            children: <Widget>[
              Container(
                padding: EdgeInsets.only(
                  top: Consts.avatarRadius + Consts.padding,
                  bottom: 8.0,
                  left: Consts.padding + 8.0,
                  right: Consts.padding + 8.0,
                ),
                margin: EdgeInsets.only(top: Consts.avatarRadius),
                decoration: new BoxDecoration(
                  color: Colors.white,
                  shape: BoxShape.rectangle,
                  borderRadius: BorderRadius.circular(3.0),
                  boxShadow: [
                    BoxShadow(
                      color: Colors.black26,
                      blurRadius: 10.0,
                      offset: const Offset(0.0, 10.0),
                    ),
                  ],
                ),
                child: Column(
                  mainAxisSize: MainAxisSize.min, // To make the card compact
                  children: <Widget>[
                    Column(
                      mainAxisSize:
                          MainAxisSize.min, // To make the card compact
                      children: <Widget>[
                        Text(
                          'Credits',
                          style: TextStyle(
                            fontSize: 24.0,
                            fontWeight: FontWeight.w700,
                          ),
                        ),
                        SizedBox(height: 16.0),
                        Text(
                          'Credits must be given where credits are due.\nIdea By: Yudi Lokhande (thanks!)\nDesigned and Developed at Regex!',
                          textAlign: TextAlign.center,
                          style: TextStyle(
                            fontSize: 16.0,
                          ),
                        ),
                        SizedBox(height: 24.0),
                        Align(
                          alignment: Alignment.bottomRight,
                          child: FlatButton(
                            onPressed: () {
                              Navigator.of(context)
                                  .pop(); // To close the dialog
                            },
                            child: Text('Okay'),
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
              ),
              Positioned(
                left: Consts.padding,
                right: Consts.padding,
                child: CircleAvatar(
                  child: Image.asset(
                    'assets/images/regex_icon_trans.png',
                    scale: 1.5,
                  ),
                  backgroundColor: Colors.black,
                  radius: Consts.avatarRadius,
                ),
              ),
            ],
          ),
        );
      });
}

void showTOC(BuildContext context) async {
  Navigator.pop(context);
  String tos = await rootBundle.loadString('docs/tos.txt');
  showDialog(
      context: context,
      builder: (context) {
        return SimpleDialog(
          title: Text('MIT Regex Terms of Service'),
          children: [
            SimpleDialogOption(
              child: Text(tos),
            ),
          ],
        );
      });
}

void showPrivacyPolicy(BuildContext context) async {
  Navigator.pop(context);
  String privacyPolicy = await rootBundle.loadString('docs/privacy_policy.txt');
  showDialog(
      context: context,
      builder: (context) {
        return SimpleDialog(
          title: Text('Privacy Policy'),
          children: [
            SimpleDialogOption(
              child: Text(privacyPolicy),
            ),
          ],
        );
      });
}

void openContactUs(BuildContext context) async {
  Navigator.pop(context);
  launch('mailto:mit.regex@gmail.com?subject=studyDump - ');
}

void shareApp(BuildContext context) async {
  Navigator.pop(context);
  Share.share(
      'Check out this app, studyDump! https://play.google.com/store/apps/details?id=com.mitregex.studydump');
}

void showDocsDialog({@required BuildContext context}) {
  showDialog(
      context: context,
      builder: (context) {
        return SimpleDialog(
          title: Text('Extra'),
          children: [
            SimpleDialogOption(
              onPressed: () => showDemo(context),
              child: Text('Tutorial'),
            ),
            SimpleDialogOption(
              onPressed: () => showCredits(context),
              child: Text('Credits'),
            ),
            SimpleDialogOption(
              onPressed: () => shareApp(context),
              child: Text('Share the App!'),
            ),
            SimpleDialogOption(
              onPressed: () => openContactUs(context),
              child: Text('Contact Us'),
            ),
            SimpleDialogOption(
              onPressed: () => showTOC(context),
              child: Text('Terms of Service'),
            ),
            SimpleDialogOption(
              onPressed: () => showPrivacyPolicy(context),
              child: Text('Privacy Policy'),
            ),
          ],
        );
      });
}

void showDriveUploadDialog(BuildContext context,
    {@required Function onPressed}) {
  showDialog(
      context: context,
      builder: (context) {
        return AlertDialog(
          title: Text('Please wait, fetching file'),
          actions: <Widget>[
            FlatButton(
              child: Text('Stop'),
              onPressed: onPressed,
            ),
          ],
          content: Container(
            height: 100,
            padding: EdgeInsets.only(top: 16.0),
            child: Center(
              child: CircularProgressIndicator(),
            ),
          ),
          contentPadding: EdgeInsets.all(32.0),
        );
      },
      barrierDismissible: false);
}

void showFileInfoDialog(
    {@required BuildContext context, @required StudyMaterial studyMaterial}) {
  print(studyMaterial.uploadTime.toString());
  String displayedDate;

  try {
    displayedDate = DateFormat.MMMMEEEEd()
        .format(DateTime.fromMillisecondsSinceEpoch(studyMaterial.uploadTime));
  } catch (e) {
    displayedDate = '';
  }

  showDialog(
      context: context,
      builder: (context) {
        return SimpleDialog(
          title: Text(studyMaterial.title),
          children: [
            SimpleDialogOption(
              child: Text('Type: ${studyMaterial.fileType}'),
            ),
            SimpleDialogOption(
              child: Text('Size: ${studyMaterial.size} bytes'),
            ),
            SimpleDialogOption(
              child: Text('Uploaded: $displayedDate'),
            ),
            SimpleDialogOption(
              child: Text('Likes: ${studyMaterial.likes}'),
            ),
            SimpleDialogOption(
              child: Text('Filters: ${studyMaterial.path}'),
            ),
          ],
        );
      });
}

Future<bool> showUploadConfirmation(
    {@required BuildContext context,
    @required String fileName,
    @required List<String> filters,
    @required Function onCancel,
    @required Function onConfirm}) async {
  return await showDialog(
      context: context,
      builder: (context) {
        return AlertDialog(
          title: Text('Confirm upload of $fileName?'),
          content: Text('To filters: ${filters.getRange(0, 3).join('/')}'),
          actions: <Widget>[
            FlatButton(child: Text('Cancel'), onPressed: onCancel),
            FlatButton(child: Text('Confirm'), onPressed: onConfirm),
          ],
        );
      });
}

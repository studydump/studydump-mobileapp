import 'dart:async';

import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

import './api/getStructure.dart';

Future<void> fetchSubjects() async {
  SharedPreferences preferences = await SharedPreferences.getInstance();
  debugPrint('Fetching new Subject list');
  await getStructure().then((res) async {
    if (res.statusCode ~/ 100 != 2) return;
    await preferences.setString('choicesData', res.body);
    await preferences.setString('DateTime', DateTime.now().toIso8601String());
    debugPrint('Subjects Fetched: ' + res.body.toString());
  });
}

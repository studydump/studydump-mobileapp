import 'dart:async';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:uri/uri.dart';

import '../../authFunctions.dart';

String getSite =
    /* Platform.isIOS
    ? 'http://localhost:3000/'
    : 'http://192.168.43.93:3000/'  */
    'http://studydump.herokuapp.com/';

http.Response handleException(http.Response res) {
  if (res.body.contains('No documents') || res.body.contains('0')) {
    return res;
  }
  if (res.statusCode ~/ 100 == 2) {
    return res;
  }
  return null;
}

Future<http.Response> requestGet(String route, String paramsAsString) async {
  String url = getSite + route + paramsAsString;
  return http.get(Uri.encodeFull(url), headers: {
    'Authorization': 'Bearer ' + await getToken(),
    'Content-Type': 'application/x-www-form-urlencoded',
  });
}

Future<http.Response> apiGet(String route, {Map<String, String> params}) async {
  String paramsAsString;
  if (params != null)
    paramsAsString = params.entries
        .fold('?', (url, param) => url + '&${param.key}=${param.value}');
  else
    paramsAsString = '';
  debugPrint('Calling API (GET): ${getSite + route + paramsAsString}');

  int retries = 10;

  http.Response res;
  try {
    res = await requestGet(route, paramsAsString);
  } catch (e) {
    debugPrint(e.toString());
    return http.Response('Connection issue', 500);
  }

  if (res.body != null && res.body.contains('Couldn\'t authenticate')) {
    await authWithToken();
  }

  while (res.statusCode ~/ 100 != 2 && retries-- != 0) {
    res = await requestGet(route, paramsAsString);
  }

  return handleException(res);
}

Future<http.Response> requestPost(String route,
    {Map<String, String> body}) async {
  return http.post(
    getSite + route,
    headers: {
      'Authorization': 'Bearer ' + await getToken(),
      'Content-Type': 'application/x-www-form-urlencoded',
    },
    body: body,
  );
}

Future<http.Response> apiPost(String route, {Map<String, String> body}) async {
  debugPrint('Calling API (POST): ${getSite + route}');
  debugPrint('\t\t' + body.toString());

  int retries = 10;

  http.Response res = await requestPost(route, body: body);

  while (res.statusCode ~/ 100 != 2 && retries-- != 0) {
    res = await requestPost(route, body: body);
  }

  return res;
}

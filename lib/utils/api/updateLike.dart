import 'package:http/http.dart' as http;

import './apiCalls.dart';

Future<http.Response> updateLike(String id, bool dislike) async {
  Map<String, String> body = {'id': id};
  if (dislike) {
    body['dislike'] = 'true';
  }

  return apiPost('updateLike', body: body);
}

import 'dart:async';

import 'package:http/http.dart' as http;

import './apiCalls.dart';

Future<http.Response> search(String query, Map<String, String> params) async =>
    apiGet('search', params: params);

import 'dart:async';

import 'package:http/http.dart' as http;

import './apiCalls.dart';

Future<http.Response> getStructure() async => apiGet('getStructure');

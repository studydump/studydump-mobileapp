import 'dart:async';

import 'package:http/http.dart' as http;

import './apiCalls.dart';

Future<http.Response> getLastModified(List<String> filters) async =>
    apiGet('lastModified', params: {
      'year': filters[0],
      'branch': filters[1],
      'subject': filters[2],
    });

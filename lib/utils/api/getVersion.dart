import 'dart:async';

import 'package:http/http.dart' as http;

import './apiCalls.dart';

Future<http.Response> getVersion() async => apiGet('getVersion');

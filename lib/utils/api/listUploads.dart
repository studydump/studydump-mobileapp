import 'package:http/http.dart' as http;

import './apiCalls.dart';

Future<http.Response> getListUploads(List<String> filters, int index) async =>
    apiGet('listUploads', params: {
      'year': filters[0],
      'branch': filters[1],
      'subject': filters[2],
      'page': index.toString(),
    });

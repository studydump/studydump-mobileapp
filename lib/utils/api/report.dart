import 'package:http/http.dart' as http;

import './apiCalls.dart';

Future<http.Response> report(String id) async {
  Map<String, String> body = {'id': id};
  return apiPost('report', body: body);
}
